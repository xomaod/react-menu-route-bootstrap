import React, { Component } from 'react';
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';


class MenuComponent extends Component {

  constructor(props) {
      super(props);

      this.toggle = this.toggle.bind(this);
      this.state = {
        active: false
      };
    }

    toggle() {
      this.setState({
        active: !this.state.active
      });
    }

   render() {
      return (
        <div className="bg-white">
        <Nav pills>
          <NavItem>
            <NavLink active={this.state.active}><Link to="/triggers" >Triggers</Link></NavLink>
          </NavItem>

          <NavItem>
            <NavLink active={this.state.active}><Link to="/commands" >Commands</Link></NavLink>
          </NavItem>

          <NavItem>
            <NavLink active={this.state.active}><Link to="/dialogs" >Dialogs</Link></NavLink>
          </NavItem>
        </Nav>
      </div>
      );
   }
}
export default MenuComponent;

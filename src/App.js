import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

import Menu from './MenuComponent';
import Triggers from './TriggersComponent';
import Commands from './CommandsComponent';
import Dialogs from './DialogsComponent';



class App extends Component {
  render() {
    return (
      <Router>
         <div className="App">
           <header className="App-header">

             <img src={logo} className="App-logo" alt="logo" />
             <h1 className="App-title">AAA Bot Command Centre</h1>

           </header>
           <Menu />

          <Route path="/triggers" component={Triggers} />
          <Route path="/commands" component={Commands} />
          <Route path="/dialogs" component={Dialogs} />

         </div>

   </Router>
    );
  }
}

export default App;

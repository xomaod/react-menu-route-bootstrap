const express = require('express');
const morgan = require('morgan');//Morgan is used for logging request details
const path = require('path');

require('dotenv').load();

const app = express();
app.use(morgan('combined'))
if(process.env.NODE_ENV == 'production'){
   app.use(express.static(path.resolve(__dirname, '..', 'build')))
   app.get('*', (req, res) => {
      res.sendFile(path.resolve(__dirname,'..', 'build', 'index.html'))
   });
}

// get the current environment
var env = process.env.NODE_ENV.toUpperCase();
var s = "EXPRESS_PORT_"+env;
const PORT = process.env[s];


app.listen(PORT, () => {
console.log(`App listening on port ${PORT}!`);
});
